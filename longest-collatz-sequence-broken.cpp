#include <iostream>
using namespace std;

long long collatz_next(long long n) 
{
    if(n % 2 == 0) 
	{
        return (n/2);
    } 
	else 
	{
        return (3*n+1);
    }
}

int collatz_length(long long start) 
{
    int count = 1;
    long long n = start;
    while(n >= 0) 
	{
        if(n == 1)
		{
			break;
		}
		n = collatz_next(n);
        count++;
    }
    return count;
}

int main() 
{
    long long max_length = 0;
    long long max_start;
    int this_length;
    for(int start = 1; start <= 1000000; start++) 
	{
        this_length = collatz_length(start);
        if(this_length > max_length) 
		{
			max_length = this_length;
            max_start = start;
			//cout << max_start << max_length << endl;
        }
    }
    cout << max_start << " is the largest possible starting number for the Collatz sequence that is less than 1000000." << endl;
}